### Interface between CV/inertial data and HMM 

import numpy as np
from HMM import computeHMMPostProb


# Interface function for internal HMM 
def runHMM(correlationScoresByIDPosVel, startProb):
	#print "Calling runHMM, startProb = ", startProb

	# Bucketize all data going into the HMM
	locationToState = generateMapsOfLocationtoState() # TODO: store so you don't need to regenerate
	locationToID, IDToLocation = generateMapsOfIDtoLocation(correlationScoresByIDPosVel) 

	# Run HMM with internal, grid-sequenced representation 
	posteriorProb = computeHMMPostProb(startProb, correlationScoresByIDPosVel, locationToState, locationToID, IDToLocation)

	# Backconvert maximum state probability to ID probability; if needed, break ties by choosing box with maximum ID
	if posteriorProb is None:
		return None, None
	else: 
		maxLikelihoodID = computeMaximumLikelihoodID(correlationScoresByIDPosVel, posteriorProb, IDToLocation, locationToState)

	# Return maximum likelihood ID and normalized probability dist over states
	return maxLikelihoodID, posteriorProb


# Helper function to account for first run of CVToInertialFunctions.py; consider moving to another module, but keep reference to functions
def computeHMMStartProb(correlationScoresByID, correlationScoresByIDPosVel, numStates=400):
	# Normalize over all locations present
	startProbByID = computeStartProb(correlationScoresByID)
	locationToState = generateMapsOfLocationtoState() # TODO: store so you don't need to regenerate
	locationToID, IDToLocation = generateMapsOfIDtoLocation(correlationScoresByIDPosVel) 

	startProb = np.zeros((1,400))

	for ID in startProbByID.keys():
		# Bucketize all data going into the HMM
		print "In startprob, ID corresponds to state: ", locationToState[IDToLocation[ID]]
		startProb[0, locationToState[IDToLocation[ID]]] = startProbByID[ID]
		
	#print "verified, sum of probs in np array = ", np.sum(startProb)
	return startProb


def computeStartProb(correlationScoresByID):
	# Normalize over all locations present
	keyset = correlationScoresByID.keys()
	total = sum(correlationScoresByID.values())

	for i in keyset:
		correlationScoresByID[i] = correlationScoresByID[i]/total

	return correlationScoresByID


# Map (x,y) location to state
#TODO: Verify that it;s doing the right thing 
def generateMapsOfLocationtoState(xVideoSize=300, yVideoSize=300, coarseness=20):
	#TODO: generalize the size of the video; for now, it assumes it is 300 x 300 (px), mapped to 20 x 20 grid

	locationToState = {}

	currentState = 0 
	numStates = 20**2 
	intervals = range(0, xVideoSize + 1, (xVideoSize/coarseness)) # +1 to capture 300 endpoint 

	# Assign all px (x,y) location to states 1 - 400 
	for xInterval in range(len(intervals) - 1):
		startXInterval =  intervals[xInterval]
		endXInterval = intervals[xInterval + 1]
		#print "raw X interval = ", xInterval, "start X Interval = ", startXInterval, ", end X Interval = ", endXInterval

		for i in range(startXInterval, endXInterval):
			for yInterval in range(len(intervals) - 1): 
				startYInterval = intervals[yInterval]
				endYInterval = intervals[yInterval + 1]
				#print "raw Y interval = ", yInterval, "state = ", (xInterval*20) + yInterval #start Y Interval = ", startYInterval, ", end Y Interval = ", endYInterval

				for j in range(startYInterval, endYInterval):
					locationToState[(i,j)] = (xInterval*20) + yInterval
					#print "Location ", (i,j), " assigned to state ", locationToState[(i,j)]

			currentState += 1

	return locationToState


def generateMapsOfIDtoLocation(correlationScoresByIDPosVel):
	locationToID = {}
	IDToLocation = {}
	#print "Matching ID to location"

	for ID in correlationScoresByIDPosVel.keys():
		rawLocation = (correlationScoresByIDPosVel[ID]['xPosition'], correlationScoresByIDPosVel[ID]['yPosition'])
		discreteLocation = (int(round(correlationScoresByIDPosVel[ID]['xPosition'])), int(round(correlationScoresByIDPosVel[ID]['yPosition'])))

		#TODO: Use this to tell how to distribute the probability mass around locations (?)
		# print "ID = ", ID, ", raw location = ", rawLocation, ", discrete location = ", discreteLocation
		locationToID[discreteLocation] = ID
		IDToLocation[ID] = discreteLocation

	return locationToID, IDToLocation	


def computeMaximumLikelihoodID(correlationScoresByIDPosVel, posteriorProb, IDToLocation, locationToState):
	likelihoodByID = {}

	for ID in correlationScoresByIDPosVel.keys():
		#convert ID to location 
		state = locationToState[IDToLocation[ID]]
		likelihoodByID[ID] = posteriorProb[0,state]

	print "likelihoods by ID = ", likelihoodByID
	print "most likely ID = ", max(likelihoodByID.iterkeys(), key=(lambda key: likelihoodByID[key]))

	return max(likelihoodByID.iterkeys(), key=(lambda key: likelihoodByID[key]))
