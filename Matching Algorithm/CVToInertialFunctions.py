### Helper methods for matching algorithm. 

import scipy.stats
import numpy
import matplotlib
from DataToHMMInterface import runHMM, computeHMMStartProb


# HMM-augmented method 
def computeIDWithHMM(cv_data, inertial_data, startTime, endTime, startProb=None):

	# Collect all the CV samples that fall within the window
	filtered_cv_data = cv_data[(cv_data[:,1] >= startTime) & (cv_data[:,1] <= endTime)] # Data is double-counted! 
	filtered_inertial_data = inertial_data[(inertial_data[:,0] >= startTime) & (inertial_data[:,0] <= endTime)]
	
	if startProb is None:
		# No previous data; return starting probability distribution weighted by correlation scores.
		correlationScoresByID = correlate(filtered_cv_data, filtered_inertial_data)
		correlationScoresByIDPosVel = correlateWithPositionAndVelocity(filtered_cv_data, filtered_inertial_data) #TODO: refactor
		startProb = computeHMMStartProb(correlationScoresByID, correlationScoresByIDPosVel)

		if correlationScoresByID: # Check if empty
			return max(correlationScoresByID.iterkeys(), key=(lambda key: correlationScoresByID[key])), startProb # Return ID for max correlation score
		else: 
			return None, None 

	else: # Run HMM-augmentation
		# TODO: Extract additional preprocessing steps; Why is this returning isnan
		correlationScoresByIDPosVel = correlateWithPositionAndVelocity(filtered_cv_data, filtered_inertial_data)

		if correlationScoresByIDPosVel:
			computedID, newStartProb = runHMM(correlationScoresByIDPosVel, startProb)
			return computedID, newStartProb 
		else:
			return None, None


def correlateWithPositionAndVelocity(filtered_cv_data, filtered_inertial_data): 
	# find unique IDs in window 
	numRows = filtered_cv_data.shape[0]
	numColumns = filtered_cv_data.shape[1]

	IDs = numpy.unique(filtered_cv_data[:,numColumns - 1])
	#print "IDs are: ", IDs

	# Segment CV data by IDs, then compute correlation score
	correlationScoresByIDPosVel = {}

	for i in range(len(IDs)):
		ID = IDs[i]
		cv_data_for_ID = filtered_cv_data[filtered_cv_data[:,numColumns - 1] == ID]
		numRowsForID = cv_data_for_ID.shape[0]

		if cv_data_for_ID.shape[0] > 1: #check if suitable to compute correlations
			correlationScore = computeCorrelation(cv_data_for_ID, filtered_inertial_data)

			#TODO: Take the last position of the correlation score or use mean position? record x, y position, and xvel and yvel
			#TODO: Generalize this schema 
			if (correlationScore is not None) and (correlationScore > 0):
				correlationScoresByIDPosVel[ID] = {'correlationScore': correlationScore, 
													'xPosition': numpy.mean(cv_data_for_ID[:, 2]), 
													'yPosition': numpy.mean(cv_data_for_ID[:, 3]), 
													'xVelocity': numpy.mean(cv_data_for_ID[:, 4]), 
													'yVelocity': numpy.mean(cv_data_for_ID[:, 5])}

	return correlationScoresByIDPosVel 


## Non-HMM method 
def computeID(cv_data, inertial_data, startTime, endTime):

	# collect all the CV samples that fall within the window
	filtered_cv_data = cv_data[(cv_data[:,1] >= startTime) & (cv_data[:,1] <= endTime)] #data is double-counted! 
	filtered_inertial_data = inertial_data[(inertial_data[:,0] >= startTime) & (inertial_data[:,0] <= endTime)]

	correlationScoresByID = correlate(filtered_cv_data, filtered_inertial_data)

	if correlationScoresByID: # check if empty
		return max(correlationScoresByID.iterkeys(), key=(lambda key: correlationScoresByID[key])) # return ID for max correlation score


def correlate(filtered_cv_data, filtered_inertial_data): 
	# find unique IDs in window 
	numRows = filtered_cv_data.shape[0]
	numColumns = filtered_cv_data.shape[1]

	IDs = numpy.unique(filtered_cv_data[:,numColumns - 1])
	#print "IDs are: ", IDs

	# Segment CV data by IDs, then compute correlation score
	correlationScoresByID = {}

	for i in range(len(IDs)):
		ID = IDs[i]
		cv_data_for_ID = filtered_cv_data[filtered_cv_data[:,numColumns - 1] == ID]

		if cv_data_for_ID.shape[0] > 1: #check if suitable to compute correlations
			correlationScore = computeCorrelation(cv_data_for_ID, filtered_inertial_data)

			#TODO: verify this
			if (correlationScore is not None) and (correlationScore > 0):
				correlationScoresByID[ID] = correlationScore

	return correlationScoresByID # return ID corresponding to most likely trajectory 


## Used by both HMM and non-HMM functions
def computeCorrelation(cv_data_for_ID, filtered_inertial_data):
	#print "in computeCorrelation, data = ", cv_data_for_ID[:,4:6], filtered_inertial_data[:,3:5]

	numRows = min(cv_data_for_ID.shape[0], filtered_inertial_data.shape[0]) # Find the number of rows in the smaller of the two sets

	# TODO: Generalize schema; rho, pValue = scipy.stats.spearmanr(cv_data_for_ID[:,3:5], filtered_inertial_data[:,1:3])
	rho, pValue = scipy.stats.spearmanr(cv_data_for_ID[0:numRows,4:6], filtered_inertial_data[0:numRows,3:5])

	if rho.size == 0: #TODO: track down the source of this.
		return None
	else: 
		return numpy.mean(rho)
