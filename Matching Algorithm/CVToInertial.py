### Basic matching algorithm for CV to inertial data fusion. 

import numpy 
import argparse
import matplotlib
import csv
from CVToInertialFunctions import computeIDWithHMM, computeID


## Parse input args
argumentParser = argparse.ArgumentParser()
argumentParser.add_argument(
    "--cv_csv",
    required=True,
    help="Path to CV CSV file.")
argumentParser.add_argument(
	"--inertial_csv",
	required=True,
	help="Path to inertial CSV file.")
argumentParser.add_argument(
	"--output_path",
	required=True,
	help="Path to output file.")
argumentParser.add_argument(
	"--window_size",
	type=int,
	required=True,
	help="Window size to compute correlations.")
argumentParser.add_argument(
	"--step_size",
	type=int,
	required=True,
	help="Step size to compute correlations.")
argumentParser.add_argument(
	'--use_hmm',
	type=int,
	required=True,
	help="Input 1 to use HMM, 0 to not use HMM.")

args = vars(argumentParser.parse_args())
print "Reading in " + args['cv_csv'] + " and " + args['inertial_csv'] + "."


## Parse CSVs: Assumes following schema
# TODO: Update with most recent
	# cv_csv: t_s, x_pos, y_pos, x_vel, y_vel, ID
	# inertial_csv: t_s, x_vel, y_vel, delta_theta

cv_data = numpy.genfromtxt(args['cv_csv'], delimiter=',', skip_header=1)
inertial_data = numpy.genfromtxt(args['inertial_csv'], delimiter=',', skip_header=1)


## Segment into windows, compute correlations, and select ID that maximizes correlation; offline version 
windowSize = args['window_size'] 
stepSize = args['step_size'] 

# Generate time series 
last_time_step = min(cv_data[cv_data.shape[0] - 1, 1], inertial_data[inertial_data.shape[0] - 1, 0]) # stop computing when either CV or inertial data terminates
first_time_step = max(cv_data[0,1], inertial_data[0,0]) # start computing when both CV and inertial data is available
time_steps = numpy.linspace(first_time_step, last_time_step, int((last_time_step - first_time_step)/stepSize) + 1) #TODO: elucidate timestamps.

# Write to output 
outputFile = open(args['output_path'], 'w') # e.g. 'test_data/walker1/walker1_eric_output.csv'
writer = csv.writer(outputFile)
numRowsWritten = 0

for x in range(time_steps.shape[0] - 1):
	#print "time to get window: " + str(int(time_steps[x])) + " until " + str(int(time_steps[x] + windowSize))
	windowStart = time_steps[x]
	windowEnd = time_steps[x] + windowSize 

	if (args['use_hmm']): #Note startprob is over states
		if x == 0:
			ID, startProb = computeIDWithHMM(cv_data, inertial_data, windowStart, windowEnd)
		else: 
			ID, startProb = computeIDWithHMM(cv_data, inertial_data, windowStart, windowEnd, startProb)	
			#wait = raw_input("PRESS ENTER TO CONTINUE.")

		# TODO: refactor this, get the mean of the cv data contained in this window
		if ID is not None: 		
			filtered_cv_data = cv_data[(cv_data[:,1] >= windowStart) & (cv_data[:,1] <= windowEnd)] #data is double-counted! 
			numColumns = filtered_cv_data.shape[1]

			if filtered_cv_data.shape[0] > 1:
				computedPosData = filtered_cv_data[filtered_cv_data[:,numColumns - 1] == ID]
				computedPosX = numpy.mean(computedPosData[:,2])
				computedPosY = numpy.mean(computedPosData[:,3]) 
				writer.writerow((windowStart, ID, computedPosX, computedPosY))
				numRowsWritten = numRowsWritten + 1 
				print "numRowsWritten = ", numRowsWritten

	else: 
		ID = computeID(cv_data, inertial_data, windowStart, windowEnd)
		print "ID = ", ID

		# TODO: refactor this, get the mean of the cv data contained in this window
		if ID is not None: 	
			filtered_cv_data = cv_data[(cv_data[:,1] >= windowStart) & (cv_data[:,1] <= windowEnd)] #data is double-counted! 
			numColumns = filtered_cv_data.shape[1]

			if filtered_cv_data.shape[0] > 1:
				computedPosData = filtered_cv_data[filtered_cv_data[:,numColumns - 1] == ID]
				computedPosX = numpy.mean(computedPosData[:,2])
				computedPosY = numpy.mean(computedPosData[:,3]) 
				writer.writerow((windowStart, ID, computedPosX, computedPosY))
				numRowsWritten = numRowsWritten + 1 
				print "numRowsWritten = ", numRowsWritten

print "number of timesteps = ", (time_steps.shape[0] - 1)
