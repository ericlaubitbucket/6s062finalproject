### Hidden Markov Model implementation for single-user tracking. 

import numpy as np 
import math 


def computeHMMPostProb(startProb, correlationScoresByIDPosVel, locationToState, locationToID, IDToLocation, numStates=400):
	#TODO: generalize the state size; for now, it assumes it is 20 X 20 grid 

	transitionProb = computeTransitionProb(startProb, IDToLocation, locationToState, correlationScoresByIDPosVel, numStates)
	emissionProb = computeEmissionProb(correlationScoresByIDPosVel, IDToLocation, locationToState, numStates)

	destinationLikelihood = np.dot(startProb, transitionProb)
	posteriorProb = np.multiply(emissionProb, destinationLikelihood)
	
	#print "Destination Likelihood = ", destinationLikelihood
	print "verify, sum of posterior prob = ", np.sum(posteriorProb)
	#print "verify, posteriorProb = ", posteriorProb/np.linalg.norm(posteriorProb)

	if np.sum(posteriorProb) == 0:
		return None 
	else:
		return posteriorProb/np.linalg.norm(posteriorProb) # Normalized probability distribution over states


## Calculating transition probability matrix 
def computeTransitionProb(startProb, IDToLocation, locationToState, correlationScoresByIDPosVel, numStates): 
	# Apply same to each state 

	transitionProb = np.zeros((numStates,numStates)) 

	# #TODO: Fuse inertial velocity here.
	# for ID in correlationScoresByIDPosVel.keys():
	# 	velocityMagnitude = np.linalg.norm(np.array([correlationScoresByIDPosVel[ID]['xVelocity'], correlationScoresByIDPosVel[ID]['yVelocity']]))
	# 	CV_velocities.append(velocityMagnitude)
	# 	averageVelocity = 

	# Get surrounding states we could transition to.
	# TODO: awaiting a priori info about state transitions
	for currentState in range(numStates): 
		destinationStates = getDestinationStates(currentState, numStates)
		probabilityByState = distributeProbabilityMass(destinationStates, currentState)

		for destinationState in destinationStates:
			transitionProb[currentState, destinationState] = probabilityByState[destinationState]
			#print "for transition from ", correspondingState, " to ", destinationState, ", assign prob of ", probabilityByState[destinationState]

	return transitionProb


def getDestinationStates(currentState, numStates):
	# Assumes states correspond to a square position grid of side state length of sqrt(numStates) and states are numbered sequentially starting at row 0, 1, ...
	surroundingStates = []

	sideLength = int(math.sqrt(numStates))
	numRows = int(currentState)/sideLength
	numCols = int(currentState % sideLength)

	surroundingStateLocations = [(numRows + 1, numCols - 1), (numRows + 1, numCols), (numRows + 1, numCols + 1),
							(numRows - 1, numCols - 1), (numRows - 1, numCols), (numRows - 1, numCols + 1),
							(numRows, numCols - 1), (numRows, numCols + 1)]

	for stateLocation in surroundingStateLocations:
		rowNum = stateLocation[0]
		colNum = stateLocation[1]

		if ((rowNum >= 0 and rowNum <= 19) and (colNum >= 0 and colNum <= 19)):
			state = (stateLocation[0]*sideLength) + stateLocation[1]
			surroundingStates.append(state)

	surroundingStates.append(currentState)
	#print "destinationStates = ", surroundingStates
	return surroundingStates


def distributeProbabilityMass(destinationStates, correspondingState, velocityMagnitude=1): #CHANGE THIS! 
	#TODO: Await a priori information to tune the distribution, for now evenly distribute across all states if the velocity isn't 0

	probabilityByState = {key: 0 for key in destinationStates}

	if velocityMagnitude == 0:
		probabilityByState[correspondingState] = 1

	else: 
		for destinationState in destinationStates:
			probabilityByState[destinationState] = 1.0/(len(destinationStates)) 
		
		# #TODO: Factor in Gaussian model 
		# xMean = IDToLocation[ID][0]
		# yMean = IDToLocation[ID][1]
		# sd = velocityMagnitude

		# if sd==0:
		# 	transition_prob[state][state] = 1
		# else: 
		# 	for i in range(state - 50, state + 50): # do a square patch around the location with half-width of 50
		# 		for j in range(state - 50, state + 50):
		# 			i = max(min(i, 90000 - 1), 0) 
		# 			j = max(min(i, 90000 - 1), 0)
		# 			transition_prob[i, j] = (calculateProbFromGaussian(stateToLocation[j][0],xMean,sd) + \
		# 				calculateProbFromGaussian(stateToLocation[j][1],yMean,sd))/2.0

	#print probabilityByState
	return probabilityByState


def calculateProbFromGaussian(x, mean, sd):
    var = float(sd)**2
    pi = 3.1415926
    denom = (2*pi*var)**.5
    num = math.exp(-(float(x)-float(mean))**2/(2*var))

    return num/denom	


## Compute emission probability vector 
def computeEmissionProb(correlationScoresByIDPosVel, IDToLocation, locationToState, numStates): 
	print correlationScoresByIDPosVel

	emissionProb = np.zeros((1, numStates)) 

	# need to make sure keys link up to discretized grid 
	for ID in correlationScoresByIDPosVel.keys():
		print "emission prob entry nonnorm for ID - ", ID, " = ", correlationScoresByIDPosVel[ID]['correlationScore']
		emissionProb[(0, locationToState[IDToLocation[ID]])] = correlationScoresByIDPosVel[ID]['correlationScore']

	#print "verify, emissionProb = ", np.sum(emissionProb/np.linalg.norm(emissionProb))
	return emissionProb/np.linalg.norm(emissionProb) #TODO: normalize or not?
