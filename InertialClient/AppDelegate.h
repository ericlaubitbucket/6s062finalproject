//
//  AppDelegate.h
//  InertialClient
//
//  Created by Eric Lau on 4/24/16.
//  Copyright © 2016 Eric Lau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

