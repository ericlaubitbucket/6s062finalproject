//
//  ViewController.h
//  InertialClient
//
//  Created by Eric Lau on 4/24/16.
//  Copyright © 2016 Eric Lau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *dataLabel;
@property (nonatomic, strong) NSInputStream *inputStream;
@property (nonatomic, strong) NSOutputStream *outputStream;
@property (nonatomic, strong) NSMutableArray *messages;

- (void) initNetworkCommunication;
- (void) messageReceived:(NSString *)message;

@end

