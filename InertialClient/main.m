//
//  main.m
//  InertialClient
//
//  Created by Eric Lau on 4/24/16.
//  Copyright © 2016 Eric Lau. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
