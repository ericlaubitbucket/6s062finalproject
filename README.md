# CV/Inertial Matching and Client/Server

## Usage
To perform CV/inertial matching, `cd` into the `Matching Algorithm` directory. Run the Python script `CVToInertial.py`, with the following required inputs. 

* `--cv_csv <path_to_computer_vision_data_csv>`
* `--inertial_csv <path_to_inertial_data_csv>`
* `--window_size` - Window size to segment and compute correlations. 
* `--step_size` - Step size to compute correlations. 