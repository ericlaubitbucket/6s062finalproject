
#include <iostream>
#include <vector>
#include <string>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/tracking.hpp>

#include "draw_utils.hpp"
#include "kalman_tracker.hpp"
#include "multi_object_tracker.hpp"
#include "contour_finder.hpp"

#include <zmq.hpp>
#include <string>
#include <iostream>
#ifndef _WIN32
#include <unistd.h>
#else
#include <windows.h>

#define sleep(n)    Sleep(n)
#endif

/**
 * Check if there's another frame in the video capture. We do this by first checking if the user has quit (i.e. pressed
 * the "Q" key) and then trying to retrieve the next frame of the video.
 */
bool hasFrame(cv::VideoCapture& capture) {
    bool hasNotQuit = ((char) cv::waitKey(1)) != 'q';
    bool hasAnotherFrame = capture.grab();
    return hasNotQuit && hasAnotherFrame;
}

/**
 * Draw the contours in a new image and show them.
 */
void contourShow(std::string drawingName,
                 const std::vector<std::vector<cv::Point>>& contours,
                 const std::vector<cv::Rect>& boundingRect,
                 cv::Size imgSize) {
    cv::Mat drawing = cv::Mat::zeros(imgSize, CV_32FC3);
    for (size_t i = 0; i < contours.size(); i++) {
        cv::drawContours(drawing,
                         contours,
                         i,
                         cv::Scalar::all(127),
                         CV_FILLED,
                         8,
                         std::vector<cv::Vec4i>(),
                         0,
                         cv::Point());
        OT::DrawUtils::drawBoundingRect(drawing, boundingRect[i]);
    }
    cv::imshow(drawingName, drawing);
}


int main(int argc, char **argv) {
    // This does the actual tracking of the objects. We can't initialize it now because
    // it needs to know the size of the frame. So, we set it equal to nullptr and initialize
    // it after we get the first frame.
    std::unique_ptr<OT::MultiObjectTracker> tracker = nullptr;

    // We'll use this variable to store the current frame captured from the video.
    cv::Mat frame;
    
    // This object represents the video or image sequence that we are reading from.
    cv::VideoCapture capture;
    
    // These two variables store the contours and contour hierarchy for the current frame.
    // We won't use the hierarchy, but we need it in order to be able to call the cv::findContours
    // function.
    std::vector<cv::Vec4i> hierarchy;
    std::vector<std::vector<cv::Point> > contours;
    
    // We'll use a ContourFinder to do the actual extraction of contours from the image.
    OT::ContourFinder contourFinder;

    // Read the first positional command line argument and use that as the video
    // source. If no argument has been provided, use the webcam.
    if (argc > 1) {
      capture.open(argv[1]);
    } else {
      capture.open(0);
    }
    
    // Ensure that the video has been opened correctly.
    if(!capture.isOpened()) {
        std::cerr << "Problem opening video source" << std::endl;
    }
    
    // Prepare context and socket
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_PUB);
    socket.bind ("tcp://*:5555");

    // Repeat while the user has not pressed "q" and while there's another frame.
    while(hasFrame(capture)) {
        // Fetch the next frame.
        capture.retrieve(frame);
        
        cv::resize(frame, frame, cv::Size(300, 300));
        
        // Create the tracker if it isn't created yet.
        if (tracker == nullptr) {
            tracker = std::make_unique<OT::MultiObjectTracker>(cv::Size(frame.rows, frame.cols));
        }
        
        // Find the contours.
        std::vector<cv::Point2f> mc(contours.size());
        std::vector<cv::Rect> boundRect(contours.size());
        contourFinder.findContours(frame, hierarchy, contours, mc, boundRect);
        
        contourShow("Contours", contours, boundRect, frame.size());
        
        // Update the predicted locations of the objects based on the observed
        // mass centers.
        std::vector<OT::TrackingOutput> predictions;
        
        tracker->update(mc, boundRect, predictions);
       
        // ID the start of a new frame; TODO: Determine message protocol.  
        printf("NewFrame\n");
        // char buffer [100];
        // sprintf(buffer, "NewFrame\n");
        // zmq::message_t reply (100);
        // memcpy (reply.data (), buffer, 100);
        // socket.send (reply);
        
        // Draw a cross for each predicted location.
        for (auto pred : predictions) {
            OT::DrawUtils::drawCross(frame, pred.location, pred.color, 5);
            OT::DrawUtils::drawTrajectory(frame, pred.trajectory, pred.color);
            
            // Send bounding box location info 
            char buffer2 [50];
            printf("%d, %d \n", pred.location.x, pred.location.y);
            sprintf(buffer2, "x = %d, y = %d", pred.location.x, pred.location.y);
            zmq::message_t reply2 (50);
            memcpy (reply2.data (), buffer2, 50);
            socket.send (reply2);
        }
        imshow("Video", frame);
    }
    return 0;
}
